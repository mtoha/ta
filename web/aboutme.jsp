<%-- 
    Document   : aboutme.jsp
    Created on : 25 Mar 10, 15:22:36
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tentang Saya</title>
    </head>
    <body>
        <table>
            <tr>
                <th>Nama</th>
                <td>Mudzakkir Toha</td>
            </tr>
            <tr>
                <th>Alamat</th>
                <td>Serengan, Solo - Surakarta</td>
            </tr>
            <tr>
                <th>Email</th>
                <td><a href="mailto:mudzakkirtoha@gmail.com">mudzakkirtoha@gmail.com</a></td>
            </tr>
        </table>
    </body>
</html>
