<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<%@ include file="/WEB-INF/jsp/taglibs.jsp"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head><meta http-equiv="Content-Language" content="English" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><decorator:title /></title>
    <link href="${base}/TASpring3PostgresLog4j/style.css" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <div id="wrap">

            <div id="header">
                <h1><a href="#">e-Comm Trading</a></h1>
                <h2>menggunakan spring dan Hibernate JPA</h2>
            </div>

            <div id="top"> </div>
            <div id="menu">
                <ul>
                    <li><a href="${base}/TASpring3PostgresLog4j/index.jsp">Home</a></li>
                    <li><a href="${base}/TASpring3PostgresLog4j/jsp/customerForm.htm">Faktur Baru</a></li>
                    <li><a href="${base}/TASpring3PostgresLog4j/jsp/barangList.htm">Daftar Barang</a></li>
                    <li><a href="${base}/TASpring3PostgresLog4j/jsp/petugasList.htm">Daftar Petugas</a></li>
                    <li><a href="${base}/TASpring3PostgresLog4j/jsp/kasirList.htm">Daftar Kasir</a></li>
                    <li><a href="${base}/TASpring3PostgresLog4j/jsp/adminList.htm">Daftar Admin</a></li>
                    <li><a href="${base}/TASpring3PostgresLog4j/jsp/barangForm.htm">Input Barang</a></li>
                    <li><a href="${base}/TASpring3PostgresLog4j/aboutme.jsp">Tentang Saya</a></li>
                </ul>
            </div>
            <div id="content">
                <div class="left">
                    <!-- BAGIAN INI akan direplace oleh isi file JSP yang lain-->
                    <decorator:body />
                </div>
                <div style="clear: both;"> </div>
            </div>
            <div id="bottom"> </div>
            <div id="footer">
                (c) 2010 e-Comm Trading
            </div>
        </div>

    </body>
</html>