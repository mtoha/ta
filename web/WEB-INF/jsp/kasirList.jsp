<%-- 
    Document   : kasirList
    Created on : 23 Mar 10, 14:04:50
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Daftar Data Kasir</title>
    </head>
    <body>
        <table>
                <tr>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Date Expired</th>
                </tr>
            <c:forEach items="${kasirList}" var="kasirs">
                <tr>
                    <td>${kasirs.nama}</td>
                    <td>${kasirs.alamat}</td>
                    <td>${kasirs.dateexpiredpetugas}</td>
                    <td><a href="kasirForm.htm?id=${kasirs.id}">edit</a></td>
                    <td><a href="kasirDelete.htm?id=${kasirs.id}">hapus</a></td>
                </tr>
            </c:forEach>
            <tr><td><a href="kasirForm.htm">Data Kasir Baru</a></td></tr>
        </table>
    </body>
</html>
