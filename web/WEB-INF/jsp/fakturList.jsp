<%-- 
    Document   : fakturList
    Created on : 23 Mar 10, 19:33:30
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <tr>
                <th>ID</th>
                <th>Nomor Faktur</th>
                <th>Tgl Faktur</th>
                <th>Customer</th>
                <th>Petugas</th>
                <th>Total Pembayaran</th>
            </tr>
            <c:forEach items="${fakturList}" var="fakturs">
                <tr>
                    <td>${fakturs.id}</td>
                    <td>${fakturs.nomorFaktur}</td>
                    <td>${fakturs.tglFaktur}</td>
                    <td>${fakturs.customer.nama}</td>
                    <td>${fakturs.petugas.nama}</td>
                    <td>${fakturs.totalPembayaran}</td>
                    <td><a href="fakturCetak.htm?id=${fakturs.id}">cetak</a></td>
                    <td><a href="fakturForm.htm?id=${fakturs.id}">edit</a></td>
                    <td><a href="fakturDelete.htm?id=${fakturs.id}">hapus</a></td>
                </tr>
            </c:forEach>
        </table>

    </body>
</html>
