<%--
    Document   : fakturForm
    Created on : 23 Mar 10, 12:33:40
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="formfaktur" %>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table width="720">
            <thead>
                <tr>
                    <th width="151"><div align="left">Nomor Faktur</div></th>
                    <th width="549"><div align="left"><c:out value="${faktur.nomorFaktur}"/></div></th>
                </tr>
            </thead>
        </table>
                <table>
                <tbody>
                    <tr><td width="48">Petugas:</td>
                        <td width="64"><c:out value="${faktur.petugas.nama}" /></td>
						<td width="328"></td>
                        <td width="109"><div align="left">Nama Customer: </div></td>
						<td width="148"><div align="right"><c:out value="${faktur.customer.nama}"/></div></td>
                    </tr>
                    <tr>
                        <td>Tanggal:</td>
						<td><c:out value="${faktur.tglFaktur}"/></td>
						<td></td>
                        <td><div align="left">Kota:</div></td>
						<td><div align="right"><c:out value="${faktur.customer.alamat}"/></div></td>
                    </tr>
                    <tr>
                        <td height="18"></td>
                        <td></td>
						<td></td>
                        <td><div align="left">No. Telp : </div></td>
						<td><div align="right"><c:out value="${faktur.customer.noTelp}"/></div></td>
                    </tr>
                </tbody>
            </table>
            <table width="715">
                  <tr>
                      <th>No</th><th>Nama Barang</th><th>Harga</th><th>Jumlah</th><th><div align="right">Sub Jumlah</div></th>
                  </tr>
                <c:forEach items="${faktur.itemBarang}" varStatus="status">
                    <tr>
                        <td>
                            <c:out value="${status.index+1}"/>
                        </td>
                        <td width="450">
                            <spring:bind path="faktur.itemBarang[${status.index}].barang.namaBarang">
                                               <c:out value="${status.value}"/>
                            </spring:bind>
                        </td>
                        <td>
                            <spring:bind path="faktur.itemBarang[${status.index}].barang.hargaJual">
                                               <c:out value="${status.value}"/>
                            </spring:bind>
                        </td>
                        <td width="45"><div align="center">
                            <spring:bind path="faktur.itemBarang[${status.index}].jumlah">
                                               <c:out value="${status.value}"/>
                            </spring:bind></div>
                        </td>
                        <td width="105"><div align="right">
                            <spring:bind path="faktur.itemBarang[${status.index}].subJumlah">
                                               <c:out value="${status.value}"/>
                            </spring:bind></div>
                        </td>
                    </tr>
                </c:forEach>
            </table>
                        <p align="right">Total : <b>Rp. <c:out value="${faktur.totalPembayaran}"/></b></p>
        <br>
        <a href="fakturList.htm">Data Faktur</a>
    </body>
</html>
