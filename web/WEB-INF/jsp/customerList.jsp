<%-- 
    Document   : customerList
    Created on : 23 Mar 10, 11:26:56
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Daftar Customer</title>
    </head>
    <body>
        <table>
            <tr>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Kota</th>
                <th>No Telp</th>
            </tr>
            <c:forEach items="${customerList}" var="p">
                <tr>
                    <td>${p.nama}</td>
                    <td>${p.alamat}</td>
                    <td>${p.kota}</td>
                    <td>${p.noTelp}</td>

                    <td><a href="customerForm.htm?id=${p.id}">edit</a></td>
                    <td><a href="customerDelete.htm?id=${p.id}">hapus</a></td>
                </tr>
            </c:forEach>
            <tr><td><a href="customerForm.htm">Data Customer Baru</a></td></tr>
        </table>
    </body>
</html>
