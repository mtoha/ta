<%-- 
    Document   : itemBarangForm
    Created on : 23 Mar 10, 20:18:25
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form:form commandName="itemBarang">
            <table>
                    <tr>
                        <td>Barang</td>
                        <td>
                            <form:select path="barang">
                                <form:options items="${barangList}" itemLabel="namaBarang" itemValue="id" />
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td>jumlah</td>
                        <td><form:input path="jumlah"/></td>
                    </tr>
                    <tr>
                        <td>Faktur</td>
                        <td>
                            <form:select path="idFaktur">
                                <form:options items="${fakturList}" itemLabel="nomorFaktur" itemValue="id" />
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><form:hidden path="subJumlah"/></td>
                    </tr>
                    <tr>
                    <td>Daftar Barang</td>
                    <td><input type="submit" value="Simpan Data"/>
                    </td>
                    <td><a href="fakturList.htm" ><input type="button" value="Proses Faktur" /></a></td>
                </tr>
            </table>
        </form:form>
        <a href="itemBarangList.htm">Data Item Barang</a>
    </body>
</html>
