<%-- 
    Document   : barangList
    Created on : 23 Mar 10, 10:34:38
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <tr>
                <th>Nama</th>
                <th>Produsen</th>
                <th>Stok Brg</th>
                <th>Harga Beli</th>
                <th>Harga Jual</th>
                <th>Kategori</th>
            </tr>
            <c:forEach items="${barangList}" var="p">
                <tr>
                    <td>${p.namaBarang}</td>
                    <td>${p.produsen}</td>
                    <td>${p.jumlah}</td>
                    <td>${p.hargaBeli}</td>
                    <td>${p.hargaJual}</td>
                    <td>${p.kategori}</td>
                    
                    <td><a href="barangForm.htm?id=${p.id}">edit</a></td>
                    <td><a href="barangDelete.htm?id=${p.id}">hapus</a></td>
                </tr>
            </c:forEach>
                <tr><td><a href="barangForm.htm">Data Barang Baru</a></td></tr>
        </table>
    </body>
</html>
