<%-- 
    Document   : customerForm
    Created on : 23 Mar 10, 10:45:18
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="formc" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <formc:form commandName="customer">
            <table>
                <tr>
                    <td>Nama</td>
                    <td><formc:input path="nama"/></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td><formc:input path="alamat"/></td>
                </tr>
                <tr>
                    <td>Kota</td>
                    <td><formc:input path="kota"/></td>
                </tr>
                <tr>
                    <td>No Telp</td>
                    <td><formc:input path="noTelp"/></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Simpan Data Customer"></td>
                </tr>
            </table>
        </formc:form>
        <a href="customerList.htm">Data Customer</a>
    </body>
</html>