<%-- 
    Document   : blocks
    Created on : 06 Apr 10, 2:12:03
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags"  prefix="spring"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:forEach items="${grid.blocks}" varStatus="gridRow">
        <spring:bind path="grid.blocks[${gridRow.index}].id">
          <c:out value="${status.value}"/>
          <input type="hidden" name="<c:out value="${status.expression}"/>"
            id="<c:out value="${status.expression}"/>"
            value="<c:out value="${status.value}"/>" />
          </spring:bind>
        <spring:bind path="grid.blocks[${gridRow.index}].description">
          <c:out value="${status.value}"/>
          <input type="hidden" name="<c:out value="${status.expression}"/>"
            id="<c:out value="${status.expression}"/>"
            value="<c:out value="${status.value}"/>" />
        </spring:bind>
        </c:forEach>
    </body>
</html>
