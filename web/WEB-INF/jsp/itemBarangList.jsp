<%-- 
    Document   : itemBarangList
    Created on : 23 Mar 10, 20:19:02
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <tr>
                <th>ID</th>
                <th>Barang</th>
                <th>Jumlah</th>
                <th>Sub Jumlah</th>
                <th>Faktur</th>
            </tr>
            <c:forEach items="${itemBarangList}" var="ib">
                <tr>
                    <td>${ib.id}</td>
                    <td>${ib.barang.namaBarang}</td>
                    <td>${ib.jumlah}</td>
                    <td>${ib.subJumlah}</td>
                    <td>${ib.idFaktur.nomorFaktur}</td>
                    <td><a href="itemBarangForm.htm?id=${ib.id}">edit</a></td>
                    <td><a href="itemBarangDelete.htm?id=${ib.id}">hapus</a></td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
