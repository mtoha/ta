<%-- 
    Document   : barangForm
    Created on : 23 Mar 10, 2:19:59
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://www.springframework.org/tags/form" prefix="formbarang" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <formbarang:form commandName="barang">
            <table>
                <tr>
                    <td>Nama</td>
                    <td><formbarang:input path="namaBarang"/></td>
                </tr>
                <tr>
                    <td>Produsen</td>
                    <td><formbarang:input path="produsen"/></td>
                </tr>
                <tr>
                    <td>Jumlah Stok Barang</td>
                    <td><formbarang:input path="jumlah"/></td>
                </tr>
                <tr>
                    <td>Harga Beli</td>
                    <td><formbarang:input path="hargaBeli"/></td>
                </tr>
                <tr>
                    <td>Harga Jual</td>
                    <td><formbarang:input path="hargaJual"/></td>
                </tr>
                <tr>
                    <td>kategori</td>
                    <td><formbarang:input path="kategori"/></td>
                </tr>
                <tr>
                    <td><input type="submit" value="simpan data barang"></td>
                </tr>
            </table>
        </formbarang:form>
        <a href="barangList.htm">Data Barang</a>
    </body>
</html>
