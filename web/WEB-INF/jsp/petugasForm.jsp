<%-- 
    Document   : petugasForm
    Created on : 04 Mar 10, 1:24:03
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.Date" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.servletsuite.com/servlets/jscalltag" prefix="j" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form:form commandName="petugas">
            <table>
                <tr>
                    <td>Nama</td>
                    <td><form:input path="Nama"/></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td><form:input path="alamat"/></td>
                </tr>
                <tr>
                    <td>User Name</td>
                    <td><form:input path="usernamepetugas"/></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><form:password path="password"/></td>
                </tr>
                <tr>
                    <td>Date Expired</td>
                    <td><form:input path="dateexpiredpetugas" /></td>
                    
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="simpan"/>
                    </td>
                </tr>
            </table>
        </form:form>
        <a href="petugasList.htm">Data Petugas</a>
    </body>
</html>
