<%-- 
    Document   : petugasList
    Created on : 06 Mar 10, 1:36:00
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Daftar Data Petugas</title>
    </head>
    <body>
        <table>
            <tr>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Date Expired</th>
            </tr>
            <c:forEach items="${petugasList}" var="p">
                <tr>
                    <td>${p.nama}</td>
                    <td>${p.alamat}</td>
                    <td>${p.dateexpiredpetugas}</td>
                    
                    <td><a href="petugasForm.htm?id=${p.id}">edit</a></td>
                    <td><a href="petugasDelete.htm?id=${p.id}">hapus</a></td>
                </tr>    
            </c:forEach>
            <tr><td><a href="petugasForm.htm">Data Baru</a></td></tr>
        </table>
    </body>
</html>
