<%-- 
    Document   : personList
    Created on : 27 Mar 10, 17:29:21
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <tr>
                <th>Nama Depan</th>
                <th>Nama Belakang</th>
                <th>Tanggal Akun</th>
            </tr>
            <c:forEach items="${personList}" var="persons">
                <tr>
                    <td>${persons.firstName}</td>
                    <td>${persons.lastName}</td>
                    <td>${persons.created}</td>
                </tr>
                <c:forEach items="${persons.addresses}" var="adresses">
                    <tr>
                        <td>${adresses.address}</td>
                        <td>${adresses.city}</td>
                        <td>${adresses.state}</td>
                        <td>${adresses.zipPostal}</td>
                    </tr>
                </c:forEach>
            </c:forEach>
        </table>

    </body>
</html>
