<%-- 
    Document   : fakturForm
    Created on : 23 Mar 10, 12:33:40
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="formfaktur" %>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <formfaktur:form commandName="faktur">
            <table>
                <tr>
                    <td>
                        <formfaktur:hidden path="id" />
                        No Faktur
                    </td>
                    <td><formfaktur:input path="nomorFaktur" readonly="false" /></td>
                </tr>
                <tr>
                    <td>Customer</td>
                    <td>
                        <formfaktur:select path="customer">
                            <formfaktur:options items="${customerList}" itemLabel="nama" itemValue="id" />
                        </formfaktur:select>
                    </td>
                </tr>
                <tr>
                    <td>Petugas</td>
                    <td>
                        <formfaktur:select path="petugas">
                            <formfaktur:options items="${petugasList}" itemLabel="nama" itemValue="id" />
                        </formfaktur:select>
                    </td>
                </tr>
                <tr>
                    <td><input type="submit" value="Simpan Data Faktur"/></td>
                </tr>
            </table>
        </formfaktur:form>
        <a href="fakturList.htm">Data Faktur</a>
    </body>
</html>
