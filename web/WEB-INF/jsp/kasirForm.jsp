<%-- 
    Document   : kasirForm
    Created on : 23 Mar 10, 13:53:13
    Author     : mudzakkir
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib  uri="http://www.springframework.org/tags/form" prefix="formkasir" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <formkasir:form commandName="kasir">
            <table>
                <tr>
                    <td>Nama</td>
                    <td><formkasir:input path="nama"/></td>
                    <td><formkasir:errors path="nama" cssClass="error"/></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td><formkasir:input path="alamat"/></td>
                    <td><formkasir:errors path="alamat" cssClass="error"/></td>
                </tr>
                <tr>
                    <td>User Name</td>
                    <td><formkasir:input path="usernamepetugas"/></td>
                    <td><formkasir:errors path="usernamepetugas" cssClass="error"/></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><formkasir:password path="password"/></td>
                    <td><formkasir:errors path="password" cssClass="error"/></td>
                </tr>
                <tr>
                    <td>Date Expired</td>
                    <td><formkasir:input path="dateexpiredpetugas" pattern="dd-MM-yyyy" /></td>
                    <td><formkasir:errors path="dateexpiredpetugas" cssClass="error"/></td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="Simpan Data Kasir" />
                    </td>
                </tr>
            </table>
        </formkasir:form>
        <a href="kasirList.htm">Data Kasir</a>
    </body>
</html>
