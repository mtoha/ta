/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.dao;

import java.util.List;
import tohasoft.model.Customer;

/**
 *
 * @author mudzakkir
 */
public interface CustomerDao {
    public void create(Customer customer);
    public void delete(Customer customer);
    public Customer getById(Long id);
    public List<Customer> getAllCustomer();
}
