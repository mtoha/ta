/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.dao;

import java.util.List;
import tohasoft.model.Administratur;

/**
 *
 * @author mudzakkir
 */
public interface AdminDao {
    public void create(Administratur administratur);
    public void delete(Administratur administratur);
    public Administratur getById(Long id);
    public List<Administratur> getAllAdmin();

}
