/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.dao;

import java.util.List;
import tohasoft.model.Kasir;

/**
 *
 * @author mudzakkir
 */
public interface KasirDao {
    public void create(Kasir kasir);
    public void delete(Kasir kasir);
    public Kasir getById(Long id);
    public List<Kasir> getAllKasir();
}
