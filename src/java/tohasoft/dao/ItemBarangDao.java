/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.dao;

import java.util.List;
import tohasoft.model.Faktur;
import tohasoft.model.ItemBarang;

/**
 *
 * @author mudzakkir
 */
public interface  ItemBarangDao {
    public void create(ItemBarang itemBarang);
    public void delete(ItemBarang itemBarang);
    public ItemBarang getById(Long id);
    public List<ItemBarang> getAllItemBarang();
}
