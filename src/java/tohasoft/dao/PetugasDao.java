/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.dao;

import java.util.List;
import tohasoft.model.Petugas;

/**
 *
 * @author mudzakkir
 */
public interface PetugasDao {
    public void create(Petugas petugas);
    public void delete(Petugas petugas);
    public Petugas getById(Long  id);
    public List<Petugas> getAllPetugas();
    public Petugas getByNama(String nama);

}
