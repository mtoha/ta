/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.dao;

import java.util.Date;
import java.util.List;
import tohasoft.model.Faktur;

/**
 *
 * @author mudzakkir
 */
public interface FakturDao {

    public void create(Faktur faktur);
    public void delete(Faktur faktur);
    public Faktur getById(Long id);
    public List<Faktur> getAllFaktur();
    public Faktur getByTanggal(Date date);
    public void  deleteItemBarang(Long id, Long itemBarangId);
}
