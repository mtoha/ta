/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.dao;

import java.util.List;
import tohasoft.model.Barang;

/**
 *
 * @author mudzakkir
 */
public interface BarangDao {
    public void create(Barang barang);
    public void delete(Barang barang);
    public Barang getById(Long id);
    public List<Barang> getAllBarang();
}
