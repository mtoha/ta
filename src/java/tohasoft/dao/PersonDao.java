/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.dao;

import java.util.Collection;
import tohasoft.model.Address;
import tohasoft.model.Person;

/**
 *
 * @author mudzakkir
 */
public interface  PersonDao {
    public Person findPersonById(Integer id);
    public Collection<Person> findPersons(final int startIndex, final int maxResults);
    public Collection<Person> findPersons() ;
    public Collection<Person> findPersonsByLastName(String lastName) ;
    public Person save(Person person) ;
    public void delete(Person person) ;
    public Person saveAddress(Integer id, Address address) ;
    public Person deleteAddress(Integer id, Integer addressId) ;
}
