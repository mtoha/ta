/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tohasoft.dao.BarangDao;
import tohasoft.model.Barang;

/**
 *
 * @author mudzakkir
 */
@Repository
@Transactional
public class BarangImpl implements BarangDao{
    @PersistenceContext
    private EntityManager em;

    public void create(Barang barang) {
        em.merge(barang);
    }

    public void delete(Barang barang) {
        em.remove(em.merge(barang));
    }

    public List<Barang> getAllBarang() {
        return em.createQuery("from Barang").getResultList();
    }

    public Barang getById(Long id) {
        return em.find(Barang.class, id);
    }

}
