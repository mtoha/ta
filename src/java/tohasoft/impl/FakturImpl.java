/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.impl;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import tohasoft.dao.FakturDao;
import tohasoft.model.Faktur;
import tohasoft.model.ItemBarang;

/**
 *
 * @author mudzakkir
 */
@Repository
@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
public class FakturImpl implements FakturDao{

    @PersistenceContext
    private EntityManager em;
    
    public void create(Faktur faktur) {
        em.merge(faktur);
    }


    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public void delete(Faktur faktur) {
            em.remove(em.merge(faktur));
    }

    public Faktur getById(Long id) {
        return em.find(Faktur.class, id);
    }

    public List<Faktur> getAllFaktur() {
        return em.createQuery("from Faktur").getResultList();
    }

    public Faktur getByTanggal(Date date) {
        return em.find(Faktur.class, date);
    }

    public void  deleteItemBarang(Long id, Long itemBarangId) {
        Faktur faktur = em.find(Faktur.class,id);
        ItemBarang itemBarang = new ItemBarang();
        itemBarang.setId(itemBarangId);
        if (faktur.getItemBarang().contains(itemBarang)) {
            faktur.getItemBarang().remove(itemBarang);
        }
        create(faktur);
    }
}
