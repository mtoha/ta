/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tohasoft.dao.KasirDao;
import tohasoft.model.Kasir;

/**
 *
 * @author mudzakkir
 */
@Repository
@Transactional
public class KasirImpl implements KasirDao{

    @PersistenceContext
    private EntityManager em;

    public void create(Kasir kasir) {
        em.merge(kasir);
    }

    public void delete(Kasir kasir) {
        em.remove(em.merge(kasir));
    }

    public Kasir getById(Long id) {
        return em.find(Kasir.class, id);
    }

    public List<Kasir> getAllKasir() {
        return em.createQuery("from Kasir").getResultList();
    }

}
