/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tohasoft.dao.AdminDao;
import tohasoft.model.Administratur;

/**
 *
 * @author mudzakkir
 */
@Repository
@Transactional
public class AdminImpl implements AdminDao{
    @PersistenceContext
    private EntityManager em;

    public void create(Administratur admin) {
        em.merge(admin);
    }

    public void delete(Administratur admin) {
        em.remove(em.merge(admin));
    }

    public Administratur getById(Long id) {
        return em.find(Administratur.class, id);
    }

    public List<Administratur> getAllAdmin() {
        return em.createQuery("from Administratur").getResultList();
    }

}
