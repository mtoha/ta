/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tohasoft.dao.CustomerDao;
import tohasoft.model.Customer;

/**
 *
 * @author mudzakkir
 */
@Repository
@Transactional
public class CustomerImpl implements CustomerDao{
    @PersistenceContext
    private EntityManager em;

    public void create(Customer customer) {
        em.merge(customer);
    }

    public void delete(Customer customer) {
        em.remove(em.merge(customer));
    }

    public List<Customer> getAllCustomer() {
        return em.createQuery("from Customer").getResultList();
    }

    public Customer getById(Long id) {
        return em.find(Customer.class, id);
    }


}
