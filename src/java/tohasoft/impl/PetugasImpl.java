
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.impl;

import tohasoft.dao.PetugasDao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import tohasoft.model.Petugas;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author mudzakkir
 */
@Repository
@Transactional
public class PetugasImpl implements PetugasDao{
    @PersistenceContext
    private EntityManager em;

    public void create(Petugas petugas) {
        em.merge(petugas);
    }

    public void delete(Petugas petugas) {
        em.remove(em.merge(petugas));
    }

    public List getAllPetugas() {
        return em.createQuery("from Petugas").getResultList();
    }

    public Petugas getById(Long  id) {
        return em.find(Petugas.class, id);
    }

    public Petugas getByNama(String nama){
        return em.find(Petugas.class, nama);
    }

}