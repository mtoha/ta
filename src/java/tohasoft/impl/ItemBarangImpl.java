/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import tohasoft.dao.ItemBarangDao;
import tohasoft.model.Faktur;
import tohasoft.model.ItemBarang;

/**
 *
 * @author mudzakkir
 */
@Repository
@Transactional
public class ItemBarangImpl implements ItemBarangDao{

    @PersistenceContext
    private EntityManager em;

    public void create(ItemBarang itemBarang) {
        em.merge(itemBarang);
    }

    public void delete(ItemBarang itemBarang) {
        em.remove(itemBarang);
    }

    public ItemBarang getById(Long id) {
        return em.find(ItemBarang.class, id);
    }

    public List<ItemBarang> getAllItemBarang() {
        return em.createQuery("from ItemBarang").getResultList();
    }

    

}
