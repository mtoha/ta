package tohasoft.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.ED2FD491-6E46-D41F-02B8-A22FA97987E1]
// </editor-fold>
@Entity
@DiscriminatorValue("1")
public class Kasir extends Petugas {

    public static final long serialVersionUID=4276734517833727032L;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.6DD5D805-7C1B-316E-7878-6D7E1C632C2E]
    // </editor-fold> 
    public Kasir () {
    }

}

