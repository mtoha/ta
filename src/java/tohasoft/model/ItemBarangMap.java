package tohasoft.model;

/**
 * Dilarang mengubah tulisan ini sebagai penghargaan anda terhadap hasil karya orang lain
 * mudzakkirtoha@gmail.com
 * teknologiinformasi1.blogspot.com
 * @author mudzakkir
 */
public class ItemBarangMap {
    private Long kd_Barang;
    private String nm_Barang;
    private Double hargaJual;
    private int jumlah;
    private Double subJumlah;

    public Double getHargaJual() {
        return hargaJual;
    }

    public void setHargaJual(Double hargaJual) {
        this.hargaJual = hargaJual;
    }

    public int getJumlah() {
        return jumlah;
    }

    public void setJumlah(int jumlah) {
        this.jumlah = jumlah;
    }

    public Long getKd_Barang() {
        return kd_Barang;
    }

    public void setKd_Barang(Long kd_Barang) {
        this.kd_Barang = kd_Barang;
    }

    

    public String getNm_Barang() {
        return nm_Barang;
    }

    public void setNm_Barang(String nm_Barang) {
        this.nm_Barang = nm_Barang;
    }

    public Double getSubJumlah() {
        return subJumlah;
    }

    public void setSubJumlah(Double subJumlah) {
        this.subJumlah = subJumlah;
    }
    
    
}
