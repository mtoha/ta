package tohasoft.model;

import java.util.Date;

/**
 * Dilarang mengubah tulisan ini sebagai penghargaan anda terhadap hasil karya orang lain
 * mudzakkirtoha@gmail.com
 * teknologiinformasi1.blogspot.com
 * @author mudzakkir
 */
public class FakturMap {
    private String kd_Kategori;
    private String nm_Kategori;
    private Date tglFaktur;
    private String customer;
    private String kota;
    private String noTelp;
    private Double total;

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getKd_Kategori() {
        return kd_Kategori;
    }

    public void setKd_Kategori(String kd_Kategori) {
        this.kd_Kategori = kd_Kategori;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getNm_Kategori() {
        return nm_Kategori;
    }

    public void setNm_Kategori(String nm_Kategori) {
        this.nm_Kategori = nm_Kategori;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

    public Date getTglFaktur() {
        return tglFaktur;
    }

    public void setTglFaktur(Date tglFaktur) {
        this.tglFaktur = tglFaktur;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

}
