package tohasoft.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;




// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.937B72D8-4A11-1481-E81D-6F1FA5E3BD93]
// </editor-fold>
@Entity
@DiscriminatorValue("2")
public class Administratur extends Petugas {
    private static final long serialversionUID=-6145145462382485655L;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.5E5F2590-8042-9C85-16A8-88AB8373CDB9]
    // </editor-fold> 
    public Administratur () {
    }

}

