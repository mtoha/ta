/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.model;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;

/**
 *
 * @author mudzakkir
 */
public class Grid {

    private List blocks =
    LazyList.decorate(
        new ArrayList(),
        FactoryUtils.instantiateFactory(Block.class));
//    private List<Block> blocks=new ArrayList<Block>();

    public List getBlocks() {
        return blocks;
    }

    public void setBlocks(List list) {
        blocks = list;
    }

}
