/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.model;

/**
 *
 * @author mudzakkir
 */
public class Block {
  private String id, description;

  public String getDescription() {
    return description;
  }

  public String getId() {
    return id;
  }

  public void setDescription(String string) {
    description = string;
  }

  public void setId(String string) {
    id = string;
  }
}
