
package tohasoft.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
//@NamedQuery(name="itembarang.mintafaktur", query="select ib from ItemBarang ib where ib.idFaktur == :faktur")
public class ItemBarang implements Serializable {
    @OneToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="idBarang")
    private Barang barang;
    private int jumlah;
    private double subJumlah;
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="idFaktur")
    private Faktur idFaktur;
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    public ItemBarang() {
    }
    
    public Barang getBarang () {
        return barang;
    }
    public void setBarang (Barang val) {
        this.barang = val;
    }
    public int getJumlah () {
        return jumlah;
    }
    public void setJumlah (int val) {
        this.jumlah = val;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Faktur getIdFaktur() {
        return idFaktur;
    }

    public void setIdFaktur(Faktur idFaktur) {
        this.idFaktur = idFaktur;
    }

    public void setSubJumlah(double subJumlah) {
        this.subJumlah = subJumlah;
    }
    
    public double getSubJumlah() {
        return subJumlah;
    }
}

