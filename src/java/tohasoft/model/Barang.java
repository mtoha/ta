package tohasoft.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;


// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.D54BE056-3883-CCE1-E836-B20290D37E8A]
// </editor-fold>
@Entity
public class Barang implements Serializable {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.37EF7279-864E-BEEB-B484-501BD7F21F36]
    // </editor-fold> 

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.8199BF1C-ED64-E2D3-0DD9-9CF230647799]
    // </editor-fold> 
    private String namaBarang;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.CD73BFB0-BDF3-01CA-B3B8-5D17C18F49A0]
    // </editor-fold> 
    private String produsen;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.5D1907E8-1FDB-7FF7-621A-766D3D75CD6B]
    // </editor-fold> 
    private int jumlah;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.78EB02A7-AC13-ABBD-CE69-D1C8B8CA2475]
    // </editor-fold> 
    private Double hargaBeli;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.3445C7EA-CFD8-B4CC-83A2-B3236446CDC0]
    // </editor-fold> 
    private java.lang.Double hargaJual;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.A9144B00-0132-002C-D665-16A810345F5D]
    // </editor-fold> 
    private String kategori;

    @OneToOne
    @JoinColumn(name="idItemBarang")
    private ItemBarang itemBarang;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    public Barang() {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.2FFA571C-605F-2400-BD51-CC19310308E9]
    // </editor-fold> 
    public Barang (String idBarang, String namaBarang, String produsen, int jumlah, Double hargaBeli, Double hargaJual, String kategori) {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.83001FEC-2DE9-8DBF-E130-E3C02F11779E]
    // </editor-fold> 
    public Double getHargaBeli () {
        return hargaBeli;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.FB022667-EFBC-9439-9538-C267FF830CE2]
    // </editor-fold> 
    public void setHargaBeli (Double val) {
        this.hargaBeli = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.54BBE4B6-6604-276D-9B97-F5ED8C6CBC7A]
    // </editor-fold> 
    public Double getHargaJual () {
        return hargaJual;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.7E570D75-5629-0196-7B4D-E14DD13D2A67]
    // </editor-fold> 
    public void setHargaJual (Double val) {
        this.hargaJual = val;
    }


    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.9BEE0F6E-65D2-C494-0CDD-131941A1011F]
    // </editor-fold> 
    public int getJumlah () {
        return jumlah;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.1596055E-ECA2-C018-3D74-53957204422F]
    // </editor-fold> 
    public void setJumlah (int val) {
        this.jumlah = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.38B4D526-3755-D740-C1DC-946354C9D9E1]
    // </editor-fold> 
    public String getKategori () {
        return kategori;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.E19668FD-6445-13BF-E3FC-FEFC6FD1DB39]
    // </editor-fold> 
    public void setKategori (String val) {
        this.kategori = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.F6CE4E03-6215-3725-138D-A57570F7E3EA]
    // </editor-fold> 
    public String getNamaBarang () {
        return namaBarang;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.F2685DD3-F5E1-A571-70AD-65EE365FE174]
    // </editor-fold> 
    public void setNamaBarang (String val) {
        this.namaBarang = val;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.57E79ECB-CB07-9378-ED2E-FA35EA879896]
    // </editor-fold> 
    public String getProdusen () {
        return produsen;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.EBC82E77-0657-963A-AB40-0B39DE4C5B63]
    // </editor-fold> 
    public void setProdusen (String val) {
        this.produsen = val;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}

