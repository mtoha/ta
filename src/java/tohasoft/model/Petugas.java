package tohasoft.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@DiscriminatorColumn(discriminatorType=DiscriminatorType.INTEGER,name="petugasTypeId")
public abstract class Petugas extends ModelBasePetugas {

    private String usernamepetugas;

    private String password;

    private String alamat;

    private String nama;

    @OneToMany(mappedBy = "petugas")
    private Set<Faktur> fakturs = new HashSet<Faktur>();


    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateexpiredpetugas;

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Date getDateexpiredpetugas() {
        return dateexpiredpetugas;
    }

    public void setDateexpiredpetugas(Date dateexpiredpetugas) {
        this.dateexpiredpetugas = dateexpiredpetugas;
    }

    public Set<Faktur> getFakturs() {
        return fakturs;
    }

    public void setFakturs(Set<Faktur> fakturs) {
        this.fakturs = fakturs;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsernamepetugas() {
        return usernamepetugas;
    }

    public void setUsernamepetugas(String usernamepetugas) {
        this.usernamepetugas = usernamepetugas;
    }

}