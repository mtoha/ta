/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.model;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author mudzakkir
 */
@MappedSuperclass

public abstract class ModelBasePetugas implements Serializable{

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

     
}
