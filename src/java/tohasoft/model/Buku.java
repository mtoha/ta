/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author mudzakkir
 */
@Entity
public class Buku {
    @Id
    private Long id;

    private String noBuku;

    public String getNoBuku() {
        return noBuku;
    }

    public void setNoBuku(String noBuku) {
        this.noBuku = noBuku;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
