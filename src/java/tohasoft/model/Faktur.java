package tohasoft.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import org.apache.commons.collections.FactoryUtils;
import org.apache.commons.collections.list.LazyList;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
@Entity
public class Faktur implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

//    @Column(length=12)
    private String nomorFaktur;

    public List<ItemBarang> getItemBarangs() {
        return itemBarangs;
    }

    public void setItemBarangs(List<ItemBarang> itemBarangs) {
        this.itemBarangs = itemBarangs;
    }
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglFaktur;

    @OneToOne
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE,
                    org.hibernate.annotations.CascadeType.DELETE})
    @JoinColumn(name="CustID")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name="PetugasID")
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE,
                    org.hibernate.annotations.CascadeType.DELETE})
    private Petugas petugas;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch=FetchType.EAGER)
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE,
                    org.hibernate.annotations.CascadeType.DELETE})
    @JoinTable(
        name="fakturitembarang",
        joinColumns = @JoinColumn( name="faktur_id"),
        inverseJoinColumns = @JoinColumn( name="itembarang_id")
    )

//    @JoinColumn(name="ItemBarang")
//    @OnDelete(action=OnDeleteAction.CASCADE)
    private List<ItemBarang> itemBarangs=LazyList.decorate(
      new ArrayList(),
      FactoryUtils.instantiateFactory(ItemBarang.class));

    private Double totalPembayaran;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<ItemBarang> getItemBarang() {
        return itemBarangs;
    }

    public void setItemBarang(List<ItemBarang> itemBarangs) {
        this.itemBarangs = itemBarangs;
    }

    public String getNomorFaktur() {
        return nomorFaktur;
    }

    public void setNomorFaktur(String nomorFaktur) {
        this.nomorFaktur = nomorFaktur;
    }

    public Petugas getPetugas() {
        return petugas;
    }

    public void setPetugas(Petugas petugas) {
        this.petugas = petugas;
    }

    public Date getTglFaktur() {
        return tglFaktur;
    }

    public void setTglFaktur(Date tglFaktur) {
        this.tglFaktur = tglFaktur;
    }

    public Double getTotalPembayaran() {
        return totalPembayaran;
    }

    public void setTotalPembayaran(Double totalPembayaran) {
        this.totalPembayaran = totalPembayaran;
    }

    public void addItemBarang(ItemBarang itemBarang){
        this.itemBarangs.add(itemBarang);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



}

