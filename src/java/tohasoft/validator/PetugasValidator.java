package tohasoft.validator;

import java.util.Date;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import tohasoft.model.Petugas;

public class PetugasValidator {

  private static final String EMAIL_FORMAT = ".*@.*\\.com";

  public void validate(Petugas administratur, Errors errors) {
    if(!StringUtils.hasText(administratur.getNama())) {
        errors.rejectValue("nama", "required", "nama harus diisi");
    }

    if (!StringUtils.hasLength(administratur.getAlamat())) {
        errors.rejectValue("alamat", "required", "alamat harus diisi");
    }

    if (!StringUtils.hasLength(administratur.getPassword())) {
        errors.rejectValue("password","required", "Password harus diisi!");
    }
    
    if(!StringUtils.hasLength(administratur.getUsernamepetugas())){
        errors.rejectValue("usernamepetugas", "required", "Username harus diisi!");
    }

    if(administratur.getDateexpiredpetugas()==null){
        errors.rejectValue("dateexpiredpetugas", "required", "harus diisi!");
    }else if(administratur.getDateexpiredpetugas().before(new Date())){
        errors.rejectValue("dateexpiredpetugas", "required", "Waktu sudah lewat!");
    }
  }
}
