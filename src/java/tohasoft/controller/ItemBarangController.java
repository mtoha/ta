/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;
import tohasoft.dao.BarangDao;
import tohasoft.dao.FakturDao;
import tohasoft.dao.ItemBarangDao;
import tohasoft.model.Barang;
import tohasoft.model.Faktur;
import tohasoft.model.ItemBarang;

/**
 *
 * @author mudzakkir"
 */
@Controller
public class ItemBarangController {

    @Autowired
    private ItemBarangDao itemBarangDao;
    @Autowired
    private  FakturDao fakturDao;
    @Autowired
    private BarangDao barangDao;

    @RequestMapping(value="/jsp/itemBarangForm.htm",method=RequestMethod.GET)
    public ModelMap getItemBarangForm(Long id,ModelMap modelMap){
        ItemBarang itemBarang;
        modelMap.addAttribute("barangList", barangDao.getAllBarang());
        modelMap.addAttribute("fakturList", fakturDao.getAllFaktur());
        if(id==null){
            itemBarang=new ItemBarang();
        }else{
            itemBarang=itemBarangDao.getById(id);
        }
        return new ModelMap(itemBarang);
    }

    @RequestMapping(value="/jsp/itemBarangForm.htm",method=RequestMethod.POST)
    public RedirectView postItemBarangForm(HttpServletRequest request){
        ItemBarang itemBarang=new ItemBarang();
        Barang barang = null;
        Faktur faktur=null;
            Long barangId;
            barangId = Long.parseLong(request.getParameter("barang"));
            int jumlah=Integer.parseInt(request.getParameter("jumlah"));
            System.out.println("Jumlah barang: "+jumlah);
            itemBarang.setJumlah(jumlah);
		if (barangId != null) {
			barang = barangDao.getById(barangId);
                        itemBarang.setBarang(barang);
                        barang.setJumlah(barang.getJumlah()-jumlah);
                        barangDao.create(barang);
                }
            Double sub=jumlah*itemBarang.getBarang().getHargaJual();
            itemBarang.setSubJumlah(sub);
            Long fakturId=null;
            fakturId=Long.parseLong(request.getParameter("idFaktur"));
            if (fakturId!=null) {
                faktur=fakturDao.getById(fakturId);
                itemBarang.setIdFaktur(faktur);
            }
            Long itembarangId = null;
            try {
                itembarangId=Long.parseLong(request.getParameter("id"));
                if (itemBarangDao.getById(itembarangId) == null) {
                    itemBarang.setId(null);
                }else{
                    itemBarang.setId(itembarangId);
                }
            } catch (Exception e) {
            }
        itemBarangDao.create(itemBarang);
        List<ItemBarang> itemBarangs=faktur.getItemBarang();
        itemBarangs.add(itemBarang);
        faktur.setItemBarang(itemBarangs);
        fakturDao.create(faktur);
        return new RedirectView("itemBarangForm.htm");
    }

    @RequestMapping(value="/jsp/itemBarangList.htm",method=RequestMethod.GET)
    public List<ItemBarang> getAllItemBarangs(){
        return itemBarangDao.getAllItemBarang();
    }

    @RequestMapping(value="/jsp/itemBarangDelete.htm",method=RequestMethod.GET)
    public RedirectView deleteItemBarangForm(@ModelAttribute ItemBarang itemBarang){
        itemBarangDao.delete(itemBarang);
        return new RedirectView("/jsp/itemBarangList.htm");
    }
}
