/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.controller;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.view.RedirectView;
import tohasoft.dao.KasirDao;
import tohasoft.model.Kasir;
import tohasoft.validator.PetugasValidator;

/**
 *
 * @author mudzakkir
 */
@Controller
public class KasirController {
    @Autowired
    private KasirDao kasirDao;

    @RequestMapping(value="/jsp/kasirForm.htm", method=RequestMethod.GET)
    public ModelMap getKasirForm(@RequestParam(value="id", required=false)Long id){
        Kasir kasir;
        if(id==null){
            kasir=new Kasir();
        }else{
            kasir=kasirDao.getById(id);
        }
        return new ModelMap(kasir);
    }

    @RequestMapping(value="/jsp/kasirForm.htm", method=RequestMethod.POST)
    public String postKasirForm(@ModelAttribute Kasir kasir,BindingResult bindingResult,SessionStatus status){
        new PetugasValidator().validate(kasir, bindingResult);
        if (bindingResult.hasErrors()) {
            return "/jsp/kasirForm";
        } else {
            if (kasir.getDateexpiredpetugas()==null) {
                Date date=new Date();
                date.setYear(date.getYear()+5);
                kasir.setDateexpiredpetugas(new Date());
            }
            kasirDao.create(kasir);
            status.setComplete();
            return "redirect:kasirList.htm";
        }
//        if(kasir.getDateexpiredpetugas()==null){
//            kasir.setDateexpiredpetugas(new Date());
//        }
//        kasirDao.create(kasir);
//        return new RedirectView("kasirList.htm");
    }

    @RequestMapping(value="/jsp/kasirList.htm", method=RequestMethod.GET)
    public List<Kasir> getAllKasir(){
        return kasirDao.getAllKasir();
    }

    @RequestMapping(value="/jsp/kasirDelete.htm",method=RequestMethod.GET)
    public RedirectView deleteKasirForm(@ModelAttribute Kasir kasir){
        kasirDao.delete(kasir);
        return new RedirectView("kasirList.htm");
    }
}
