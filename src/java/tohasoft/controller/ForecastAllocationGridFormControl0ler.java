/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponseWrapper;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import tohasoft.model.Grid;

/**
 *
 * @author mudzakkir
 */
public class ForecastAllocationGridFormControl0ler
  extends SimpleFormController {

  public ForecastAllocationGridFormControl0ler() {
    setCommandName("grid");
    setCommandClass(Grid.class);
  }

  protected Object formBackingObject(HttpServletRequest request)
    throws Exception {
    Grid grid = new Grid();
    if (!isFormSubmission(request)) {
//      grid = CreateGrid.expensiveToCall();
    }
    return grid;
  }

  public ModelAndView onSubmit(
    HttpServletRequest request,
    HttpServletResponseWrapper response,
    Object command,
    BindException errors)
    throws Exception {

    Grid grid = (Grid) command;
    //hey it's fully populated

    return new ModelAndView(getSuccessView());
  }
}
