/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.controller;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tohasoft.dao.PersonDao;
import tohasoft.model.Person;

/**
 *
 * @author mudzakkir
 */
@Controller
public class PersonController {
    @Autowired
    private PersonDao personDao;

    @RequestMapping(value="/jsp/personList.htm",method=RequestMethod.GET)
    public Collection<Person> getAllPersons(){
        return personDao.findPersons();
    }

}
