/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.controller;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.view.RedirectView;
import tohasoft.dao.AdminDao;
import tohasoft.model.Administratur;
import tohasoft.validator.PetugasValidator;

/**
 *
 * @author mudzakkir
 */
@Controller
public class AdminController {
    @Autowired
    private AdminDao adminDao;

    @RequestMapping(value="/jsp/adminForm.htm",method=RequestMethod.GET)
    public ModelMap getAdminForm(@RequestParam(value="id", required=false)Long id){
        Administratur administratur;
        if(id==null){
            administratur=new Administratur();
        }else{
            administratur=adminDao.getById(id);
        }
        return new ModelMap(administratur);
    }

    @RequestMapping(value="/jsp/adminForm.htm", method=RequestMethod.POST)
    public String postAdminForm(@ModelAttribute Administratur administratur,BindingResult bindingResult,SessionStatus status){
        new PetugasValidator().validate(administratur, bindingResult);
        if (bindingResult.hasErrors()) {
            return "/jsp/adminForm";
        } else {
            if (administratur.getDateexpiredpetugas()==null) {
                Date date=new Date();
                date.setYear(date.getYear()+5);
                administratur.setDateexpiredpetugas(new Date());
            }
            adminDao.create(administratur);
            status.setComplete();
            return "redirect:adminList.htm";
        }        
    }

    @RequestMapping(value="/jsp/adminList.htm", method=RequestMethod.GET)
    public List<Administratur> getAllAdmin(){
        return adminDao.getAllAdmin();
    }

    @RequestMapping(value="/jsp/adminDelete.htm",method=RequestMethod.GET)
    public RedirectView deleteAdminForm(@ModelAttribute Administratur administratur){
        adminDao.delete(administratur);
        return new RedirectView("adminList.htm");
    }
}
