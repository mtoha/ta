/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import tohasoft.dao.CustomerDao;
import tohasoft.model.Customer;

/**
 *
 * @author mudzakkir
 */
@Controller
public class CustomerController {

    @Autowired
    private CustomerDao customerDao;

    @RequestMapping(value="/jsp/customerForm.htm",method=RequestMethod.GET)
    public ModelMap getCustomerForm(@RequestParam(value="id",required=false)Long id){
        Customer customer;
        if(id==null){
            customer=new Customer(){};
        }else{
            customer=customerDao.getById(id);
        }
        return new ModelMap(customer);
    }

    @RequestMapping(value="/jsp/customerForm.htm",method=RequestMethod.POST)
    public RedirectView postCustomerForm(@ModelAttribute Customer  customer){
        customerDao.create(customer);
        return new RedirectView("fakturForm.htm");
    }


    @RequestMapping(value="/jsp/customerList.htm",method=RequestMethod.GET)
    public List<Customer> getAllCustomer(){
        return customerDao.getAllCustomer();
    }

    @RequestMapping(value="/jsp/customerDelete.htm",method=RequestMethod.GET)
    public RedirectView deleteCustomerForm(@ModelAttribute Customer customer){
        customerDao.delete(customer);
        return new RedirectView("customerList.htm");
    }
}
