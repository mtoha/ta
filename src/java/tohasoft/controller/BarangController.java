package tohasoft.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import java.util.List;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import tohasoft.dao.BarangDao;
import tohasoft.model.Barang;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mudzakkir
 */
@Controller
public class BarangController {
    @Autowired
    private BarangDao barangDao;

    @RequestMapping(value="/jsp/barangForm.htm",method=RequestMethod.GET)
    public ModelMap getPetugasForm(@RequestParam(value="id", required=false)Long  id){
        Barang barang;
        if(id==null){
            barang=new Barang() {};
        }else{
            barang=barangDao.getById(id);
        }
        return new ModelMap(barang);
    }

    @RequestMapping(value="/jsp/barangForm.htm", method=RequestMethod.POST)
    public RedirectView postPetugasForm(@ModelAttribute Barang barang){
        barangDao.create(barang);
        return new RedirectView("barangList.htm");
    }

    @RequestMapping(value="/jsp/barangList.htm", method=RequestMethod.GET)
    public List<Barang> getAllPetugas(){
        return barangDao.getAllBarang();
    }

    @RequestMapping(value="/jsp/barangDelete.htm",method=RequestMethod.GET)
    public RedirectView deleteBarangForm(@ModelAttribute Barang barang){
        barangDao.delete(barang);
        return new RedirectView("barangList.htm");
    }
}
