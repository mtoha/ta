/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package tohasoft.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.view.JasperViewer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;
import tohasoft.dao.BarangDao;
import tohasoft.dao.CustomerDao;
import tohasoft.dao.FakturDao;
import tohasoft.dao.ItemBarangDao;
import tohasoft.dao.PetugasDao;
import tohasoft.model.Barang;
import tohasoft.model.Customer;
import tohasoft.model.Faktur;
import tohasoft.model.ItemBarang;
import tohasoft.model.ItemBarangMap;
import tohasoft.model.Petugas;
import tohasoft.NewClass;
import tohasoft.model.FakturMap;
import tohasoft.supportobject.AutoNumber;

/**
 *
 * @author mudzakkir
 */
@Controller
public class FakturController {
    @Autowired
    private FakturDao fakturDao;
    @Autowired
    private CustomerDao  customerDao;
    @Autowired
    private PetugasDao petugasDao;
    @Autowired
    private BarangDao barangDao;
    @Autowired
    private ItemBarangDao itemBarangDao;

    @RequestMapping(value="/jsp/fakturForm.htm",method=RequestMethod.GET)
    public ModelMap getFakturForm(Long id,ModelMap modelMap){
        Faktur faktur = null;
        List<Customer> listCustomers=customerDao.getAllCustomer();
        modelMap.addAttribute("customerList", customerDao.getAllCustomer());
        List<Petugas> listPetugases=petugasDao.getAllPetugas();
        modelMap.addAttribute("petugasList", listPetugases);
        List<Barang> listBarangs=barangDao.getAllBarang();
        modelMap.addAttribute("barangList", listBarangs);
//        List<ItemBarang> listItemBarangs=itemBarangDao.getAllItemBarang();
//        modelMap.addAttribute(fakturDao);
        Date date=new Date();
        modelMap.addAttribute("tglFaktur",date);
//        ItemBarang itemBarang;
        modelMap.addAttribute("barangList", barangDao.getAllBarang());
//        modelMap.addAttribute("fakturList", fakturDao.getAllFaktur());
//        if(id==null){
//            itemBarang=new ItemBarang();
//        }else{
//            itemBarang=itemBarangDao.getById(id);
//        }
        if(id==null){
            List<ItemBarang> itemBarangs=new ArrayList<ItemBarang>();
            itemBarangs.add(new ItemBarang());
            itemBarangs.add(new ItemBarang());
            itemBarangs.add(new ItemBarang());
            Date date1=new Date();
            faktur=new Faktur();
            faktur.setItemBarang(itemBarangs);
            faktur.setNomorFaktur(
                    String.valueOf(date1.getDate())+"."+
                    String.valueOf(date1.getMonth()+1)+"."+
                    date1.toString().substring(date1.toString().length()-4, date1.toString().length())+"-"+
                    String.valueOf(date1.getSeconds())+"."+
                    String.valueOf(date1.getMinutes())+"."+
                    String.valueOf(date1.getHours())+"."+
                    String.valueOf(AutoNumber.getAutonumber()));
        }else{
            faktur=fakturDao.getById(id);
        }
        if(faktur.getCustomer()==null){
        }
        return new ModelMap(faktur);
    }

    @RequestMapping(value="/jsp/fakturDelete.htm",method=RequestMethod.GET)
    public RedirectView deleteFakturForm(@ModelAttribute Faktur faktur){
        fakturDao.delete(faktur);
        return new RedirectView("fakturList.htm");
    }

    @RequestMapping(value="/jsp/fakturForm.htm",method=RequestMethod.POST)
    public RedirectView postFakturForm(HttpServletRequest request){
        Faktur faktur=new Faktur();
        try {
            Long customerId;
            customerId = Long.parseLong(request.getParameter("customer"));
		if (customerId != null) {
			Customer customer = customerDao.getById(customerId);
                        faktur.setCustomer(customer);
                        System.out.print(customer.toString());
                }
            Long petugasId=null;
            petugasId=Long.parseLong(request.getParameter("petugas"));
            if (petugasId!=null) {
                Petugas petugas=petugasDao.getById(petugasId);
                faktur.setPetugas(petugas);
                System.out.print(petugas.toString());
            }
            Date date=new Date();
            faktur.setTglFaktur(date);
            faktur.setNomorFaktur(request.getParameter("nomorFaktur"));
            Long fakturId;
            fakturId=Long.parseLong(request.getParameter("id"));
            if (fakturDao.getById(fakturId) == null) {
                faktur.setId(null);
            }else{
                faktur.setId(fakturId);
            }
        } catch (Exception e) {
            System.out.println("Penyimpanan gagal karena: "+e);
        }
        fakturDao.create(faktur);
        return new RedirectView("itemBarangForm.htm");
    }

    @RequestMapping(value="/jsp/fakturList.htm",method=RequestMethod.GET)
    public List<Faktur> getAllFakturs(){
        return fakturDao.getAllFaktur();
    }

    @RequestMapping(value="/jsp/fakturCetak.htm", method=RequestMethod.GET)
    public RedirectView getFaktur(Long id,HttpServletResponse response) throws JRException, ServletException, IOException{
        Faktur faktur=fakturDao.getById(id);
        List<ItemBarang> itemBarangs=faktur.getItemBarang();
        Double fakturjumlah=0.0;
        
        /* Update Jumlah total Pembelian */
        for (int i = 0; i < faktur.getItemBarang().size(); i++) {
            fakturjumlah=fakturjumlah+faktur.getItemBarang().get(i).getSubJumlah();
        }
        faktur.setTotalPembayaran(fakturjumlah);
        faktur.setTotalPembayaran(fakturjumlah);
        fakturDao.create(faktur);
        List<ItemBarangMap> listItemBarangMaps=new ArrayList<ItemBarangMap>();
        int itemBarangSize=fakturDao.getById(id).getItemBarangs().size();
        Faktur faktur1=fakturDao.getById(id);

        /* Proses Wrapping model ItemBarang*/
        for (int i = 0; i < fakturDao.getById(id).getItemBarangs().size(); i++) {
            ItemBarangMap itemBarangMap=new ItemBarangMap();
            itemBarangMap.setKd_Barang(faktur1.getItemBarangs().get(i).getBarang().getId());
            itemBarangMap.setNm_Barang(faktur1.getItemBarangs().get(i).getBarang().getNamaBarang());
            itemBarangMap.setJumlah(faktur1.getItemBarangs().get(i).getJumlah());
            itemBarangMap.setHargaJual(faktur1.getItemBarangs().get(i).getBarang().getHargaJual());
            itemBarangMap.setSubJumlah(faktur1.getItemBarangs().get(i).getSubJumlah());
            listItemBarangMaps.add(itemBarangMap);
        }

        /* Sudah mendapat object Kategori beserta data barangnya,
         * sekarang siapkan parameter untuk reportnya */
        Map<String, Object> parameter = new HashMap<String, Object>();

        /* Sekarang tidak perlu lagi mencari report path, report bisa
         * dicemplungin ke dalam jar sekalian ^_^ */
        parameter.put("SUBREPORT_DIR",
                NewClass.class.getResourceAsStream(
                "/tohasoft/report/RptDetail.jasper"));

        /* Mengisi detail report */
        parameter.put("PRM_DETAIL_VALUE", listItemBarangMaps);

        /* Proses compilasi report */
        JasperReport jp = JasperCompileManager.compileReport(
            NewClass.class.getResourceAsStream(
            "/tohasoft/report/RptMaster.jrxml"));

        /* Proses Wrapping model Faktur*/
        List<FakturMap> fakturMaps=new ArrayList<FakturMap>();
        FakturMap fakturMap=new FakturMap();
        fakturMap.setKd_Kategori(faktur1.getNomorFaktur());
        fakturMap.setTglFaktur(faktur1.getTglFaktur());
        fakturMap.setKota(faktur1.getCustomer().getKota());
        fakturMap.setCustomer(faktur1.getCustomer().getNama());
        fakturMap.setNm_Kategori(faktur1.getPetugas().getNama());
        fakturMap.setNoTelp(faktur1.getCustomer().getNoTelp());
        fakturMap.setTotal(faktur1.getTotalPembayaran());
        fakturMaps.add(fakturMap );

        /* Proses pengisian report dengan data */
        JasperPrint jasperPrint = JasperFillManager.fillReport(jp, parameter,
            new JRBeanCollectionDataSource(fakturMaps));

        /* Proses export ke PDF */
        OutputStream ouputStream = response.getOutputStream();
        JRExporter exporter = null;
        response.setContentType("application/pdf");
        exporter = new JRPdfExporter();
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
        try {
                exporter.exportReport();
        } catch (JRException e) {
                throw new ServletException(e);
        } finally{
                if (ouputStream != null) {
                        try	{
                                ouputStream.close();
                        } catch (IOException ex) {

                        }
                }
        }
        return new RedirectView("/jsp/fakturList.htm");
    }
}
