package tohasoft.controller;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import tohasoft.dao.PetugasDao;
import java.util.List;
import tohasoft.model.Petugas;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mudzakkir
 */
@Controller
public class PetugasController {
    @Autowired
    private PetugasDao petugasDao;

    @RequestMapping(value="/jsp/petugasForm.htm",method=RequestMethod.GET)
    public ModelMap getPetugasForm(@RequestParam(value="id", required=false)Long  id){
        Petugas petugas;
        if(id==null){
            petugas=new Petugas() {};
        }else{
            petugas=petugasDao.getById(id);
        }
        return new ModelMap(petugas);
    }

    @RequestMapping(value="/jsp/petugasForm.htm", method=RequestMethod.POST)
    public RedirectView postPetugasForm(@ModelAttribute Petugas petugas){
        petugasDao.create(petugas);
        return new RedirectView("petugasList.htm");
    }

    @RequestMapping(value="/jsp/petugasList.htm", method=RequestMethod.GET)
    public List<Petugas> getAllPetugas(){
        return petugasDao.getAllPetugas();
    }

    @RequestMapping(value="/jsp/petugasDelete.htm",method=RequestMethod.GET)
    public RedirectView deletePetugasForm(@ModelAttribute Petugas petugas){
        petugasDao.delete(petugas);
        return new RedirectView("petugasList.htm");
    }
}
